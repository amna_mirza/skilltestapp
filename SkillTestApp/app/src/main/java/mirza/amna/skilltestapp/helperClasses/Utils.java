package mirza.amna.skilltestapp.helperClasses;

import android.content.Context;
import android.content.res.Configuration;
import android.widget.Toast;

/**
 * Created by amna.mirza on 6/1/2016.
 */
public class Utils {
    private Utils() {
    }

    /**
     * Show Toast Message
     *
     * @param context
     * @param message
     */
    public static void showToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /**
     * Check the device if mobile or tablet
     *
     * @param context
     */
    public static boolean isTab(Context context) {
        if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= 4)
            return true;
        else return false;
    }

}
